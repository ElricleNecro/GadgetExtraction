# Gadget Extraction

This program will extract Gadget file and output them as text files.

## Limitations

- Only support the gadget format 1.
- Only extract basic information (positions, velocities, ids, types, masses).
- Output files is determined as <infile>.txt
