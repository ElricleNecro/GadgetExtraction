#include <IOGadget/gadget.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
	if( argc == 1 ) {
		fprintf(stderr, "You have to give one or more files.\n");
		fprintf(stderr, "Usage: %s file1 [file2 [file3 ...]]\n", argv[0]);
		exit(EXIT_FAILURE);
	}

	for(int i=1; i < argc; i++) {
		char *output = malloc((strlen(argv[i]) + 5) * sizeof(char));
		sprintf(output, "%s.txt", argv[i]);

		// First, we read the gadget file:
		Header header;
		Particule_d particules = Double_Gadget_Read_format1(
			argv[i],
			&header,
			1,
			false,
			false,
			false,
			false
		);

		// We get the total number of particule:
		int n_total = 0;
		for(int type=0; type < 6; type++) {
			n_total += header.npart[type];
		}

		// We output them:
		FILE *fout = NULL;
		if( (fout = fopen(output, "w")) == NULL ) {
			perror("Problem while opening the output file:");
			free(output);
			exit(EXIT_FAILURE);
		}

		fprintf(fout, "# x y z vx vy vz m id type\n");
		for(int idx=0; idx < n_total; idx++) {
			fprintf(fout, "%g %g %g %g %g %g %g %d %d\n",
				particules[idx].Pos[0],
				particules[idx].Pos[1],
				particules[idx].Pos[2],
				particules[idx].Vit[0],
				particules[idx].Vit[1],
				particules[idx].Vit[2],
				particules[idx].m,
				particules[idx].Id,
				particules[idx].Type
			);
		}

		printf("Input: %s\n", argv[i]);
		printf("Output: %s\n", output);

		fclose(fout);
		free(particules);
		free(output);
	}

	return EXIT_SUCCESS;
}
