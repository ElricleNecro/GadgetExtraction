CC       = gcc
CXX      = g++

BIN      = gadextract
VERSION  = 0.1

SRCEXT   = c
SRCDIR   = src
INCEXT   = h
INCDIR   = include
OBJDIR   = build
BINDIR   = build
LIBDIR   = build

PREFIX   = $$HOME/.local

SRCS    := $(shell find $(SRCDIR) -name '*.$(SRCEXT)')
HDRS    := $(shell find $(INCDIR) -name '*.$(INCEXT)')
SRCDIRS := $(shell find . -name '*.$(SRCEXT)' -exec dirname {} \; | uniq)
INCDIRS := $(shell find . -name '*.$(INCEXT)' -exec dirname {} \;)
OBJS    := $(patsubst %.$(SRCEXT),$(OBJDIR)/%.o,$(SRCS))

DEBUG    = -g3
INCLUDES = -Iinclude/
CFLAGS   = -W -Wall -Wshadow -Wcast-qual -Wcast-align -Wsign-compare -Wstrict-prototypes -Wredundant-decls -Wnested-externs -Wunreachable-code -Wwrite-strings -pedantic -fstrict-aliasing $(DEBUG) $(INCLUDES) -c
LDFLAGS  = $(shell pkg-config --cflags --libs iogadget)

ifeq ($(SRCEXT), cpp)
CC       = $(CXX)
else
CFLAGS  += -std=gnu99
endif

.PHONY: all clean distclean test install

all: $(BINDIR)/$(BIN)

test:
	@echo $(SRCS)
	@echo $(SRCDIRS)
	@echo $(OBJS)
	@echo $(CFLAGS)
	@echo $(LDFLAGS)

install: $(BINDIR)/$(BIN)
	install -m 755 $< $(PREFIX)/bin

$(BINDIR)/$(BIN): buildrepo $(OBJS)
	@mkdir -p `dirname $@`
	@echo "Linking $@..."
	@$(CC) $(LDFLAGS) $(OBJS) -o $@

$(OBJDIR)/%.o: %.$(SRCEXT)
	@echo "Generating dependencies for $<..."
	@$(call make-depend,$<,$@,$(subst .o,.d,$@))
	@echo "Compiling $<..."
	@$(CC) $(CFLAGS) $< -o $@

clean:
	$(RM) -r $(OBJDIR)

distclean: clean
	$(RM) -r $(BINDIR)

buildrepo:
	@$(call make-repo)

uninstall:
	$(RM) $(PREFIX)/bin/$(BIN)

define make-repo
   for dir in $(SRCDIRS); \
   do \
	mkdir -p $(OBJDIR)/$$dir; \
   done
endef

# usage: $(call make-depend,source-file,object-file,depend-file)
define make-depend
  $(CC) -MM       \
        -MF $3    \
        -MP       \
        -MT $2    \
        $(CFLAGS) \
        $1
endef
